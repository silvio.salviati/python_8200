# 1) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.


def evogal(letra):
    if letra.lower() in "aeiouáéíóúãẽĩõũâêîôûàèìòùäëïöü":
        return True
    else:
        return False


vogais = []

with open('faroeste', 'r') as letra:
    arquivo = letra.readlines()
    for linha in arquivo:
        for word in linha.split(' '):
            for letter in word:
                if evogal(letter) == True:
                    vogais.append(letter)
                
print(len(vogais))

