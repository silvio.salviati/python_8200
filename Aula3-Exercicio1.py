# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:



def saudacao(nome):
    print(f'''
    Nome da função: saudacao("{nome}")
    retorno: "Olá {nome}! Tudo bem com você?"
    ''')

n = input('Digite seu nome? ')
saudacao(n)
