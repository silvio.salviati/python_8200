# 2) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;


import csv


cpf = input('Qual o cpf da pessoa? ')
nome = input('Qual o nome da pessoa? ')
idade = input('Qual a idade da pessoa? ')
sexo = input('Qual o sexo da pessoa? ')
cidade = input('Qual a cidade onde ela mora? ')

dados = [cpf, nome, idade, sexo, cidade]

with open('csv', 'w') as arquivo:
     conteudo = csv.writer(arquivo, delimiter=';')
     conteudo.writerow(dados)
