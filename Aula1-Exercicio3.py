# 3) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

n1 = int(input('digite um numero inteiro: '))
n2 = int(input('digite mais 1 numero: '))

soma = n1 + n2
diff = n1 - n2

print(f'''Soma: {n1} + {n2} = {soma}
Diferença: {n1} - {n2} = {diff}
''')
