# 1) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

geracao = int(input('Qual ano você nasceu? '))

if geracao <= 1964:
    print(f'Ano nascimento = {geracao}: Geração: Baby Boomer')
elif geracao <= 1979:
    print(f'Ano nascimento = {geracao}: Geração: X')
elif geracao <= 1994:
    print(f'Ano nascimento = {geracao}: Geração: Y')
else:
    print(f'Ano nascimento = {geracao}: Geração: Z')
