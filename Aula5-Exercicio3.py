# 3) Implemente uma função de consulta no programa do exercício 2.


import csv


def consulta():
    opcao = int(input('''Escolha a informação a ser consultada?
    1 - CPF
    2 - Nome
    3 - Idade
    4 - Sexo
    5 - Cidade
    '''))

    with open('csv', 'r') as arquivo:
        arq = csv.reader(arquivo, delimiter=';')
        valor = next(arq)
        if opcao == 1:
            print(f'O CPF é {valor[0]}')
        if opcao == 2:
            print(f'O nome é{valor[1]}')
        if opcao == 3:
            print(f'A Idade é {valor[2]}')
        if opcao == 4:
            print(f'O Sexo é {valor[3]}')
        if opcao == 5:
            print(f'A Cidade é {valor[4]}')


cpf = input('Digite o CPF? ')
nome = input('Digite o nome? ')
idade = input('Digite a idade? ')
sexo = input('Digite o sexo? ')
cidade = input('Digite a cidade? ')

base = [cpf, nome, idade, sexo, cidade]

with open('csv', 'w') as arquivo:
     conteudo = csv.writer(arquivo, delimiter=';')
     conteudo.writerow(base)

retorno = input('Deseja consultar algum dado do cadastro feito? [S/N]').upper()

if retorno == 'S':
    consulta()
