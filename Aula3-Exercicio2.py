# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.


def calculadora(valor1, valor2, acao):
    if acao == 1:
        resultado = valor1 + valor2
        print(f'O resultado da soma entre {valor1} e {valor2} é {resultado}')
    elif acao == 2:
        resultado = valor1 - valor2
        print(f'O resultado da diferença entre {valor1} e {valor2} é {resultado}')
    elif acao == 3:
        resultado = valor1 * valor2
        print(f'O resultado da multiplicação entre {valor1} e {valor2} é {resultado}')
    elif acao == 4:
        resultado = valor1 / valor2
        print(f'O resultado da divisão de {valor1} por {valor2} é {resultado}')
    else:
        resultado = "Numero invalido"


n1 = float(input("Digite 1 numero: "))
n2 = float(input("Digite mais 1 numero: "))

escolha = int(input(f'''
Escolha uma operação:
1 - Somá-los
2 - Subtraí-los
3 - Multiplicá-los
4 - Dividí-los
'''))

calculadora(n1, n2, escolha)
