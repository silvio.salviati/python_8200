# Exercicio 4:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.


def pares(lista):
    resultado = 0

    for numero in lista:
        if numero % 2 == 0:
            resultado += 1
    
    print(f'Numeros pares da lista {resultado}.')

numeros = []
while True:
    escolha = int(input('Digite um numero, ou digite 0 para sair: '))
    if escolha == 0:
        break
    numeros.append(escolha)

pares(numeros)
