# Exercicio 5:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.


def spray(x, y):
    area = x * y
    latas = (area / 3)
    
    print(f'É necessário {latas:.2f} latas de tinta.')


altura = float(input('Qual a altura da parede? '))
largura = float(input('Qual a largura da parede? '))

spray(altura, largura)
