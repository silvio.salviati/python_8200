# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o pacote random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: Você poderá utilizar o pacote time para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".


from random import randint
from time import sleep

participantes = ['Roberto', 'Baltazar', 'Jão', 'Primo', 'Leo', 'Levi', 'Thomas', 'Iracema', 'Bigorna', 'Barro']

print('*' * 20)
print('Que os jogos comecemmmm... hahahahahah')
for x in range(0, 9):
    escolhido = participantes[randint(0, len(participantes) - 1)]
    print(f'O eliminado foi {escolhido}!')
    participantes.remove(escolhido)
    print(f'Jogadores restantes: {participantes}')
    sleep(2)
    print('Nova Rodada!')

print(f'E o vencedor foi... {participantes[0]} !!')
