# 4) Implemente uma função de exclusão no programa do exercício 2.


import csv


def remover(dados):
    opcao = int(input('''digite a informação a ser removida?
    1 - CPF
    2 - Nome
    3 - Idade
    4 - Sexo
    5 - Cidade
    '''))

    with open('csv', 'w') as arquivo:
        conteudo = csv.writer(arquivo, delimiter=';')
        dados.pop(opcao - 1)
        conteudo.writerow(dados)

        if opcao == 1:
            print(f'O CPF excluido com sucesso!!!')
        if opcao == 2:
            print(f'O NOME excluido com sucesso!!!')
        if opcao == 3:
            print(f'A IDADE excluido com sucesso!!!')
        if opcao == 4:
            print(f'O SEXO excluido com sucesso!!!')
        if opcao == 5:
            print(f'A CIDADE excluido com sucesso!!!')


cpf = input('Qual o cpf da pessoa? ')
nome = input('Qual o nome da pessoa? ')
idade = input('Qual a idade da pessoa? ')
sexo = input('Qual o sexo da pessoa? ')
cidade = input('Qual a cidade onde ela mora? ')

dados = [cpf, nome, idade, sexo, cidade]

with open('csv', 'w') as arquivo:
     conteudo = csv.writer(arquivo, delimiter=';')
     conteudo.writerow(dados)

resposta = input('Deseja remover algum dado do cadastro feito? [S/N]').upper()

if resposta == 'S':
    remover(dados)
