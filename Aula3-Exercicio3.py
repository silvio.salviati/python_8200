# Exercicio 3:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações 
# em funções.


def verCesta():
    print(cesta)


def menuFrutas():
    fruta = int(input('''
    Quitanda do baltazar:

    Escolha uma opção de fruta:
    1 - Banana
    2 - Melancia
    3 - Morango

    Digite a opção desejada:
    '''))

    if fruta == 1:
        cesta.append('Banana')
        print('Banana adicionada com sucesso!')
    elif fruta == 2:
        cesta.append('Melancia')
        print('Melancia adicionada com sucesso!')
    elif fruta == 3:
        cesta.append('Morango')
        print('Morango adicionada com sucesso!')
    else:
        print('Codigo inexistente "{fruta}"')    
    

def checkout():
    preco = 0
    for x in cesta:
        if x == "Banana":
            preco += 3.50
        elif x == "Melancia":
            preco += 7.50
        else:
            preco += 5.00

    print(f'''
    Total de compras: {preco:.2f}
    Cesta de compras: {cesta}
    ''')


cesta = []
opcao = 0
fruta = 0

while opcao != 4:
    opcao = int(input('''
===================
Quitanda:
1 - Ver cesta
2 - Adicionar Frutas
3 - Checkout
4 - Sair

Digite a opção desejada: 
    '''))

    if opcao == 1:
        verCesta()

    if opcao == 2:
        menuFrutas()

    if opcao == 3:
        checkout()
